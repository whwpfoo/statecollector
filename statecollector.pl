use strict;
use warnings;
use lib "./lib";
use Try::Tiny;
use StateCollector::Engine::Client;
use StateCollector::Engine::Collector;
use StateCollector::Utils::Helper;

sub main {
    my ($command) = @ARGV;

    if ($command) {
        die "Automation must be run as root.\n" if ($> != 0);

        my $exec = {"run" => "StateCollector::Engine::Client"};

        try {
            my $socket = $exec -> {$command} -> new();
        }

        catch {
            die "[!] ERROR: this command could not be run\n";
        };

        return 1;
    }
    
    return print StateCollector::Utils::Helper -> new();
}

main();
exit;