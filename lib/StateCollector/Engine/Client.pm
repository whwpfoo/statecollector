package StateCollector::Engine::Client {
    use strict;
    use warnings;
    use Try::Tiny;
    use Config::Simple;
    use IO::Socket;
    use JSON;
    use Data::Dumper qw(Dumper);
    use StateCollector::Engine::Collector;

    sub new {
        my $config       = Config::Simple -> new("./env/host.conf");
        my $hostname     = $config -> param("host_addr");
        my $hostport     = $config -> param("host_port");
        my $hostprotocol = $config -> param("host_protocol");

        try {
            my $socket = _connection($hostname, $hostport, $hostprotocol);
            my $kidpid = fork();     # 프로세스로 분리

            $socket -> autoflush(1); # 출력 시 바로 송신 설정
            
            unless (defined($kidpid)) {
                die "[!] ERROR Can't Fork: $!"; 
            }

            print "> Sucess Socket Connection -> $hostname\n";
            print "> First HandShake...\n";

            #my @enable_options = _handshake($socket);
            #my $collector = StateCollector::Engine::Collector -> new(@enable_options);
            my $collector = StateCollector::Engine::Collector -> new();

            # 원격지 응답 수신
            if ($kidpid) {
                while (_isconnected($socket)) {
                    my $status = <$socket>;
                    print "> Remote Status -> $status\n";
                }

                kill("TERM" => $kidpid);
            }

            # 원격지 요청 송신
            else {
                while (my $systeminfo = $collector -> run()) {

                    unless (defined($systeminfo)) {
                        die "[!] Failed Collect System Info !\n";
                    }

                    my @json_array    = map { $systeminfo -> {$_} } keys(%$systeminfo);
		    my $json_tostring = encode_json(\@json_array);

                    print "> Remote Push Data -> ", $collector -> get_time(), "\n";
                    print $socket($json_tostring);
                    print $socket("\n");

                    sleep(3);
                }
            }
        }

        catch {
            print "$_\n";
        };

        print "> Disconnection Socket Server...\n";
        return 1;
    }

    sub _connection {
        my ($hostname, $hostport, $hostprotocol) = @_;
        my $socket = IO::Socket::INET -> new(
            PeerAddr => $hostname,
            PeerPort => $hostport,
            Proto    => $hostprotocol,
        ) || die "[!] Fail Connection: $!\n";

        return $socket;
    }

    sub _handshake {
        my ($socket) = @_;

        print $socket("init");
        print $socket("\n");

        my @enables = split(/,/, <$socket>);

        return @enables;
    }

    sub _isconnected {
	my ($socket) = @_;
        return $socket -> connected();
    }
}

1;
