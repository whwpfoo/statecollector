package StateCollector::Engine::Collector {
    use strict;
    use warnings;
    use Linux::Info;
    use JSON;

    sub new {
        my ($class, $use_cpu, $use_mem, $use_disk, $use_netstat) = @_;
        
        my $collector = Linux::Info -> new();

        $collector -> set(
                            cpustats  => $use_cpu     //  1,
                            memstats  => $use_mem     //  1,
                            diskusage => $use_disk    //  1,
                            netstats  => $use_netstat //  1,
                         );


        return bless { "_collector" => $collector }, $class;
    }

    sub set_option {
        my ($self, $use_cpu, $use_mem, $use_disk, $use_netstat) = @_;

        $self -> {"_collector"} -> set(
                                         cpustats  => $use_cpu     // 1,
                                         memstats  => $use_mem     // 1,
                                         diskusage => $use_disk    // 1,
                                         netstats  => $use_netstat // 1,
                                      );

        return 1;
    }

    sub get_time {
        my ($self) = @_;
        return $self -> {"_collector"} -> gettime('%Y/%m/%d^%H:%M:%S');
    }

    sub run {
        my ($self) = @_;
        return $self -> {"_collector"} -> get();
    }
}

1;
